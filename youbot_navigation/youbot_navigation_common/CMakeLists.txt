cmake_minimum_required(VERSION 2.8.3)
project(youbot_navigation_common)

find_package(catkin REQUIRED roscpp geometry_msgs genmsg actionlib_msgs actionlib tf)

generate_messages(DEPENDENCIES actionlib_msgs)

catkin_package()

add_executable(
lowpass_filter src/lowpass_filter.cpp
)
add_executable(
drift_solver src/youbot_drift_solver.cpp
)


target_link_libraries(lowpass_filter ${catkin_LIBRARIES})
target_link_libraries(drift_solver ${catkin_LIBRARIES})
