/*
 * youbot autonomous drift solving interface
 * Author: Sandesh Gowda
 * Created: 07/15/2015
 */

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <math.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
#include <actionlib/client/simple_action_client.h>

#include <termios.h>
#include <signal.h>

using namespace std;

class driftSolverYoubot
{
public:
	driftSolverYoubot( ros::NodeHandle &node )  ;
	geometry_msgs::PoseStamped goalPt;
	geometry_msgs::PoseStamped prevGoalPt;
	geometry_msgs::PoseStamped basePt;
private:
	ros::Subscriber goal_sub ;
	ros::Subscriber feedback_sub ;
	ros::Publisher vel_pub ;
	ros::Timer cmd_timer;
	void positionCheck(const move_base_msgs::MoveBaseActionGoal::ConstPtr&);
	void youbotBasefeedback(const move_base_msgs::MoveBaseActionFeedback::ConstPtr&);
	void goalReachedCmd(const ros::TimerEvent&);
	actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> m_moveBaseClient;
	
	string m_transformFrame ;
	string m_fixedFrame ;
	tf::TransformListener m_listener ;
	
	int stopCount;
	bool goalSet;
};

driftSolverYoubot::driftSolverYoubot( ros::NodeHandle &node ): m_moveBaseClient("move_base", true)
{
	goal_sub =  node.subscribe("/move_base/goal", 100, &driftSolverYoubot::positionCheck, this);
	feedback_sub =  node.subscribe("/move_base/feedback", 1000, &driftSolverYoubot::youbotBasefeedback, this);
	vel_pub = node.advertise<geometry_msgs::Twist>("/cmd_vel",100);
	cmd_timer = node.createTimer(ros::Duration(0.05), &driftSolverYoubot::goalReachedCmd,this);
	m_transformFrame = "odom" ;
	m_fixedFrame = "map" ;
	
	goalSet = false;
	prevGoalPt.pose.position.x = 0;
	prevGoalPt.pose.position.y = 0;
	stopCount = 0;

}

void driftSolverYoubot::positionCheck(const move_base_msgs::MoveBaseActionGoal::ConstPtr &newGoal)
{
	//goalPt = newGoal->goal.target_pose;
	//ROS_INFO("Goal point is:: %lf %lf",goalPt.pose.position.x,goalPt.pose.position.y);
	try
	{
		m_listener.transformPose(m_transformFrame,ros::Time(0),newGoal->goal.target_pose,m_fixedFrame,goalPt);
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s",ex.what());
	}
	if(prevGoalPt.pose.position.x != newGoal->goal.target_pose.pose.position.x || prevGoalPt.pose.position.y != newGoal->goal.target_pose.pose.position.y)
	{
		goalSet = true;
		prevGoalPt.pose.position.x = newGoal->goal.target_pose.pose.position.x;
		prevGoalPt.pose.position.y = newGoal->goal.target_pose.pose.position.y;
	}
}

void driftSolverYoubot::youbotBasefeedback(const move_base_msgs::MoveBaseActionFeedback::ConstPtr & youbotfeed)
{
	basePt = youbotfeed->feedback.base_position;
	ROS_DEBUG("X position of Base point is:: %lf",basePt.pose.position.x);
	ROS_DEBUG("The status is ::: %d",youbotfeed->status.status);
}

void driftSolverYoubot::goalReachedCmd(const ros::TimerEvent&)
{
	double radius = fabs(sqrt(pow((goalPt.pose.position.x - basePt.pose.position.x),2) + pow((goalPt.pose.position.y - basePt.pose.position.y),2)));
	ROS_DEBUG("Distance from goal is:: %lf",radius);
	if( radius < 0.1 && goalSet == true && stopCount<5)//or c == STOPCODE_ENTER)
	{
		ROS_DEBUG("Goal Reached");
		geometry_msgs::Twist msg;
		msg.linear.x = 0.0;
		msg.linear.y = 0.0;
		msg.angular.z = 0.0;
		vel_pub.publish(msg);
		stopCount++;
//		cout<<"Stop Count"<<stopCount<<endl;
		if(stopCount==5)
		{
			goalSet = false;
			stopCount = 0;
		}
	}
	
}

//Main Function Call
int main(int argc, char **argv)
{
	ros::init(argc, argv, "drift_solver_youbot");
	ros::NodeHandle n;

	driftSolverYoubot solver(n);
	//signal(SIGINT,quit);
	ros::spin();
	
	return 0;
}

