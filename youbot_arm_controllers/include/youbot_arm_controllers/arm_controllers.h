/*
 * arm_controllers.h
 *
 *  Created on: Jul 30, 2015
 *      Author: Sandesh Gowda
 */

#ifndef YOUBOT_ARM_CONTROLLER_H_
#define YOUBOT_ARM_CONTROLLER_H_

#include <ros/ros.h>

#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <cortex_msgs/ControlType.h>

#include <brics_actuator/JointTorques.h>
#include <brics_actuator/JointPositions.h>
#include <brics_actuator/JointVelocities.h>

#include <boost/units/systems/si.hpp>
#include <boost/units/io.hpp>


class ArmControllers
{
	private:
		ros::NodeHandle nh;

		// Publishers
		ros::Publisher armControllerPub;

		// Services
		ros::ServiceServer toggle_srv;
		ros::ServiceServer jointSelector_srv;
		bool toggleSrvCB(cortex_msgs::ControlType::Request &req, cortex_msgs::ControlType::Response &res);
		bool jointSelectorCB(cortex_msgs::ControlType::Request &req, cortex_msgs::ControlType::Response &res);

		// Subscribers
		ros::Subscriber tabletOrientationSub;
		ros::Subscriber jointSub;
		void tabletOrientationCB(const geometry_msgs::Twist::ConstPtr& msg);
		void jointStateCB(const sensor_msgs::JointState::Ptr& msg);

		// Timers
		ros::Timer armCmdTimer;
		void armTimerCB(const ros::TimerEvent&);

		////////////////////////
		// Tablet status
		double rate;
		double yaw;
		double roll;
		double pitch;

		bool joint_pitch_init;

		double* minJointValues;
		double* maxJointValues;
		double* jointGains;

		//Tablet-specific variables
		double *tablet_orientation; //[0]:yaw;[1]:pitch;[2]:roll

		double gripper_displacement;
		double arm_displacement;
		double platter_displacement;
		////////////////////////

		// Functions
		void initArmPosition();
		void limitControl(brics_actuator::JointPositions& position_command);
		void controllerOffCB();
		void individualJointCB();
		void combinedJointCB();

		double tabletThreshold;
		double jointStepSize;

		// Controller status
		enum mode {OFF,individualJoint,combinedJoint} controlSelected;
		enum joints {ARM_1,ARM_2,ARM_3,ARM_4,ARM_5} jointSelected;
		bool controllerSwitched;

		// Data storage
		double* currentJointValues;
		bool updatedCurrentJointValues;

		// ROS messages
		brics_actuator::JointPositions armPosCommand;
		std::vector<brics_actuator::JointValue> armPositionsBrics;

	public:

		ArmControllers();
		~ArmControllers();

};


#endif /* YOUBOT_FORCE_BASE_CONTROLLER_H_ */
