/*
 * arm_controller.cpp
 *
 *  Created on: Jul 30, 2015
 *      Author: Sandesh Gowda
 */

#include <youbot_arm_controllers/arm_controllers.h>

ArmControllers::ArmControllers()
{
	rate = 0.02 ;
	jointStepSize = 0.012;
	tabletThreshold = 30.0;	// TODO read from yaml

	armControllerPub = nh.advertise<brics_actuator::JointPositions>("/arm_1/arm_controller/position_command",1);

	toggle_srv = nh.advertiseService("/controllers/arm", &ArmControllers::toggleSrvCB, this);
	jointSelector_srv = nh.advertiseService("/controllers/arm/jointSelector", &ArmControllers::jointSelectorCB, this);

	tabletOrientationSub = nh.subscribe("/arm_vel",1,&ArmControllers::tabletOrientationCB,this);
	jointSub = nh.subscribe("/joint_states", 1, &ArmControllers::jointStateCB,this);

	armCmdTimer = nh.createTimer(ros::Duration(rate), &ArmControllers::armTimerCB, this);

	// Tablet
	tablet_orientation  = new double[3];

	// Joint limits
	minJointValues    = new double[5];
	maxJointValues    = new double[5];

	minJointValues[0] = 0.0100693;				// TODO have separate for individal joint controller
	minJointValues[1] = 0.49062;				// TODO check if new position will make gripper collide using tf tree or moveit
	minJointValues[2] = -3.26034;
	minJointValues[3] = 0.77665;
	minJointValues[4] = 0.110620;

//	maxJointValues[0] = 5.84013;
//	maxJointValues[1] = 2.09893;
//	maxJointValues[2] = -1.602165;
//	maxJointValues[3] = 2.43484;
//	maxJointValues[4] = 5.6415;

	maxJointValues[0] = 5.84013;
	maxJointValues[1] =  2.60;
	maxJointValues[2] = -2.00;
	maxJointValues[3] =  2.49;
	maxJointValues[4] = 5.6415;

	//Joint Pitch Gains
	jointGains 	  = new double[5];
//	jointGains[0] = 0.005;
//	jointGains[1] = 0.05;
//	jointGains[2] = 0.05;
//	jointGains[3] = 0.05;
//	jointGains[4] = 0.05;
	jointGains[0] = 0.005;					// TODO load from yaml
	jointGains[1] = 0.020;
	jointGains[2] = 0.020;
	jointGains[3] = 0.020;
	jointGains[4] = 0.025;


	// Controller mode
	controlSelected = ArmControllers::OFF;
	jointSelected = ArmControllers::ARM_1;

	// Arm message
	armPositionsBrics.resize(5);
	for(int i=0;i<5;i++)
	{
		armPositionsBrics[i].joint_uri 	= "arm_joint_" + boost::lexical_cast<std::string>(i+1);
		armPositionsBrics[i].unit 		= boost::units::to_string(boost::units::si::radians);
		armPositionsBrics[i].value 		= 0.0;
	}

	currentJointValues = new double[5];	// TODO init
	updatedCurrentJointValues = false;

	controllerSwitched = true;
}

ArmControllers::~ArmControllers()
{
	if(!currentJointValues)
		delete currentJointValues;
	if(!jointGains)
		delete jointGains;
	if(!maxJointValues)
		delete maxJointValues;
	if(!minJointValues)
		delete minJointValues;
	if(!tablet_orientation)
		delete tablet_orientation;
}

/*********************************** TOPICS ***********************************/

void ArmControllers::jointStateCB(const sensor_msgs::JointState::Ptr& msg)
{
	int i=0;
	for(std::vector<std::string>::const_iterator it = msg->name.begin(); it != msg->name.end(); ++it)
	{
		if((*it) == "arm_joint_1") {						// TODO this can be improved
			currentJointValues[0] = msg->position[i];
		}
		else if ((*it) == "arm_joint_2") {
			currentJointValues[1] = msg->position[i];
		}
		else if ((*it) == "arm_joint_3") {
			currentJointValues[2] = msg->position[i];
		}
		else if ((*it) == "arm_joint_4") {
			currentJointValues[3] = msg->position[i];
		}
		else if ((*it) == "arm_joint_5") {
			currentJointValues[4] = msg->position[i];
		}
		i++;
	}
	updatedCurrentJointValues = true;
}

void ArmControllers::tabletOrientationCB(const geometry_msgs::Twist::ConstPtr& msg)
{
	tablet_orientation[0] = msg->linear.x; 	// yaw: 0 -> 360
	tablet_orientation[1] = msg->linear.y; 	// pitch: -90 -> 90 (up is negative)
	tablet_orientation[2] = msg->angular.z; // roll: -90 -> 90 (clockwise is negative)

	yaw   = tablet_orientation[0];
	pitch = tablet_orientation[1];
	roll  = tablet_orientation[2];

	//std::cout<<"R="<<roll<<"\n"<<"P="<<pitch<<"\n"<<"Y="<<yaw<<"\n";

	if(std::fabs(pitch)<30)
		pitch=0;
	if(std::fabs(roll)<30)
		roll=0;

	arm_displacement = (pitch/3)*M_PI/180;
	gripper_displacement = (roll/3)*M_PI/180;
	platter_displacement = yaw;						// FIXME
}

/*********************************** SERVICES ***********************************/

bool ArmControllers::jointSelectorCB(cortex_msgs::ControlType::Request &req, cortex_msgs::ControlType::Response &res)
{
	switch (req.type)
	{
		case 0:
			jointSelected = ArmControllers::ARM_1;
			break;
		case 1:
			jointSelected = ArmControllers::ARM_2;
			break;
		case 2:
			jointSelected = ArmControllers::ARM_3;
			break;
		case 3:
			jointSelected = ArmControllers::ARM_4;
			break;
		case 4:
			jointSelected = ArmControllers::ARM_5;
			break;
		default:
			ROS_WARN("YouBotArm::jointSelectorCB - request invalid!");
			break;
	}
	//std::cout<<"ArmControllers::jointSelectorCB - selected " << (int)jointSelected << "\n";
	return true;
}

bool ArmControllers::toggleSrvCB(cortex_msgs::ControlType::Request &req, cortex_msgs::ControlType::Response &res)
{
	ArmControllers::mode tmp = controlSelected;

	switch (req.type)
	{
		case 0:
			controlSelected = ArmControllers::OFF;
			break;
		case 1:
			controlSelected = ArmControllers::individualJoint;
			break;
		case 2:
			controlSelected = ArmControllers::combinedJoint;
			break;
		default:
			ROS_WARN("YouBotArm::toggleSrvCB - request invalid!");
			break;
	}

	//std::cout<<"ArmControllers::toggleSrvCB - selected " << (int)controlSelected << "\n";

	if(tmp != controlSelected)
		controllerSwitched = true;

	return true;
}

/*********************************** FUNCTIONS  ***********************************/

void ArmControllers::initArmPosition()
{
	/******Move to candle position******/
	armPositionsBrics[0].value = 2.9496;			// TODO make this a switch with arm pose enum
	armPositionsBrics[1].value = 1.1344;
	armPositionsBrics[2].value = -2.5482;
	armPositionsBrics[3].value = 1.789;
	armPositionsBrics[4].value = 2.9234;

	armPosCommand.positions = armPositionsBrics;
	armControllerPub.publish(armPosCommand);
}

void ArmControllers::armTimerCB(const ros::TimerEvent&)
{
	if(controllerSwitched && controlSelected!=ArmControllers::OFF)
	{
		initArmPosition();
		controllerSwitched = false;
		return;							// TODO wait for youbot to reache pose?
	}

	switch (controlSelected)
	{
		case ArmControllers::OFF:
			controllerOffCB();
			break;
		case ArmControllers::individualJoint:
			individualJointCB();
			break;
		case ArmControllers::combinedJoint:
			combinedJointCB();
			break;
		default:
			ROS_WARN("YouBotArm::armTimerCB - no controller selected!");
			break;
	}
}

void ArmControllers::controllerOffCB()
{
	// Do nothing?
}

void ArmControllers::individualJointCB()
{
	double delta;

	if(tablet_orientation[1] < -tabletThreshold)
	{
		delta = jointStepSize;	// TODO make this a variable
	}
	else if(tablet_orientation[1] > tabletThreshold)
	{
		delta = -jointStepSize;
	}
	else
	{
		return;
	}

	// Update and publish command
	armPositionsBrics[(int)jointSelected].value +=delta;		// Assume other values are still the same
	armPosCommand.positions = armPositionsBrics;
	limitControl(armPosCommand);
	armPositionsBrics = armPosCommand.positions;				// Update saved positions

	ros::Time t = ros::Time::now();
	for(int i=0;i<5;i++)
		armPosCommand.positions[i].timeStamp = t;

	armControllerPub.publish(armPosCommand);
}

void ArmControllers::combinedJointCB()
{
	//platterMovement
	armPosCommand.positions[0].value += 0; //FIXME

	//armMovement
	armPosCommand.positions[1].value += (jointGains[1]*arm_displacement);
	armPosCommand.positions[2].value += (jointGains[2]*arm_displacement);
	armPosCommand.positions[3].value += (jointGains[3]*arm_displacement);

	//EndEffectorMovement
	armPosCommand.positions[4].value += (jointGains[4]*gripper_displacement);
	limitControl(armPosCommand);

	ros::Time t = ros::Time::now();
	for(int i=0;i<5;i++)
		armPosCommand.positions[i].timeStamp = t;

	armControllerPub.publish(armPosCommand);
}

void ArmControllers::limitControl(brics_actuator::JointPositions& position_command)
{
	for(int i = 0; i < 5; i++)
	{
		if(position_command.positions[i].value < minJointValues[i])
		{
			position_command.positions[i].value = minJointValues[i];
		}
		if(position_command.positions[i].value > maxJointValues[i])
		{
			position_command.positions[i].value = maxJointValues[i];
		}
	}
}

/*********************************** MAIN ***********************************/

int main(int argc, char** argv)
{
	ros::init(argc,argv,"arm_controllers");
	ros::NodeHandle nh;

	ArmControllers armMovement;

	ros::spin();

	return 0;
}
