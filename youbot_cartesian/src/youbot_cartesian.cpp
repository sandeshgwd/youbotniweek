/*
 * youbot_cartesian.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: Sven Cremer
 */

#include <ros/ros.h>
#include <iostream>
#include <string>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

// Youbot drivers
#include <brics_actuator/JointVelocities.h>
#include <brics_actuator/JointValue.h>
#include <brics_actuator/JointPositions.h>

// ROS messages
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Int8.h>
#include <cortex_msgs/SetBool.h>

// Boost
#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/conversion.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/plane_angle.hpp>
#include <boost/units/systems/si/angular_velocity.hpp>

using Eigen::MatrixXd;


Eigen::VectorXd velocity;
ros::Publisher velPub;
ros::Publisher posPub;
ros::Subscriber jointSub;
ros::Subscriber tabletRotSub;
ros::Subscriber tabletZSub;

bool receivedJointValues;
bool controllerEnabled;

std::vector<std::string> joint_names;
double* joint_values;

double roll;
double pitch;
double yaw;

int up_down_state;

void move_arm_to_start_position();

double t_z;
double z_max;
double z_min;

Eigen::Affine3d starting_pose;
bool saved_starting_pose=false;

/*********************************** Subscribers ***********************************/

void joint_state_CB(const sensor_msgs::JointState::Ptr& msg)
{
	int i=0;
	for(std::vector<std::string>::const_iterator it = msg->name.begin(); it != msg->name.end(); ++it)
	{
		if((*it) == "arm_joint_1") {						// TODO this can be improved
			joint_values[0] = msg->position[i];
			receivedJointValues = true;
		}
		else if ((*it) == "arm_joint_2") {
			joint_values[1] = msg->position[i];
		}
		else if ((*it) == "arm_joint_3") {
			joint_values[2] = msg->position[i];
		}
		else if ((*it) == "arm_joint_4") {
			joint_values[3] = msg->position[i];
		}
		else if ((*it) == "arm_joint_5") {
			joint_values[4] = msg->position[i];
		}
		i++;
	}
}

void tablet_orientation_CB(const geometry_msgs::Twist::Ptr& msg)
{
	yaw 	= msg->linear.x;		// 0 -> 360
	pitch 	= msg->linear.y;		// -90 -> 90 (up is negative)
	roll	= msg->angular.z;		// -90 -> 90 (clockwise is negative)
}

/*********************************** TABLET CB ***********************************/

void up_down_selector_CB(const std_msgs::Int8::Ptr msg)
{
	if((int)msg->data==-1)			// TODO check
	{
		up_down_state = -1;
	}
	else if((int)msg->data==1)
	{
		up_down_state = 1;
	}
}

bool toggle_srv_CB(cortex_msgs::SetBool::Request &req, cortex_msgs::SetBool::Response &res)
{
//  bool tmp = controllerEnabled;
  if(!req.variable)
  {
	  controllerEnabled = false;
	  ROS_INFO("DISABLED cartesian arm controller!");
  }
  else
  {

	  ROS_INFO("ENABLED cartesian arm controller!");
//	  if(tmp!=controllerEnabled)							// TODO check
	  move_arm_to_start_position();
	  controllerEnabled = true;
  }

  return true;
}

/*********************************** Functions ***********************************/

void move_arm_to_start_position()
{
	brics_actuator::JointPositions position_command;
	position_command.positions.resize(5);
	for (int i = 0; i < 5; ++i)
	{
		position_command.positions[i].joint_uri = "arm_joint_" + boost::lexical_cast<std::string>(i+1);
		position_command.positions[i].unit = boost::units::to_string(boost::units::si::radians);
	}
	position_command.positions[0].value = 2.9496;
	position_command.positions[1].value = 1.800;
	position_command.positions[2].value = -2.224;
	position_command.positions[3].value = 2.4249;
	position_command.positions[4].value = 2.9234;
	posPub.publish(position_command);

	saved_starting_pose=false;

	sleep(2.0);	// TODO remove
}

/*********************************** MAIN ***********************************/

int main(int argc, char** argv)
{
	//Initialize ROS
	ros::init (argc, argv, "youbot_cartesian");
	ros::NodeHandle n;

	//Initialize Moveit
	robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
	robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
	ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
	robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));
	const robot_state::JointModelGroup* joint_model_group = kinematic_model->getJointModelGroup("arm_1");
	const std::vector<std::string> &joint_names = joint_model_group->getJointModelNames();

	//ROS Publisher
	velPub = n.advertise<brics_actuator::JointVelocities>("arm_1/arm_controller/velocity_command", 1);
	posPub = n.advertise<brics_actuator::JointPositions>("arm_1/arm_controller/position_command", 1);

	//ROS Subscribers
	jointSub = n.subscribe("/joint_states", 1, joint_state_CB);
	tabletRotSub = n.subscribe("/arm_vel", 1, tablet_orientation_CB);
	tabletZSub = n.subscribe("/up_down_selector",1,up_down_selector_CB);
	//sub_3 = n.subscribe("/joystick_pose", 1, joystick_pose_CB);

	// ROS services
	ros::ServiceServer toggle_srv = n.advertiseService("/controllers/cartesian", toggle_srv_CB);

	ros::Rate loop_rate(50);

	//move_arm_to_start_position();

	// Initialize command vector
	std::cout << "Init command vector" << std::endl;
	brics_actuator::JointVelocities command;
	command.velocities.resize(5);
	for(int i=0;i<5;i++)
	{
		command.velocities[i].joint_uri = "arm_joint_" + boost::lexical_cast<std::string>(i+1);
		command.velocities[i].value = 0;
		command.velocities[i].unit = boost::units::to_string(boost::units::si::radians_per_second);
	}

	//Get joint values
	joint_values = new double[5];
	while(!receivedJointValues)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}

	controllerEnabled = false;
	// Main loop
	ROS_INFO("Starting main loop");
	while(ros::ok())
	{
		ros::spinOnce();

		if(controllerEnabled)
		{
			/*****Update moveit robot state*****/
			for(int i=0;i<5;i++)
			{
				const double tmp = joint_values[i];
				kinematic_state->setJointPositions(joint_names[i].c_str(),&tmp);
			}

			/*****Forward Kinematics*****/
			//		std::cout << "*****Break 2*****" << std::endl;
			const Eigen::Affine3d &end_effector_state = kinematic_state->getGlobalLinkTransform("gripper_finger_link_l");
			//const Eigen::Affine3d &end_effector_state = kinematic_state->getGlobalLinkTransform("arm_link_5");
			Eigen::Affine3d new_end_effector = end_effector_state;
			Eigen::Affine3d old_end_effector = end_effector_state;

			if(!saved_starting_pose)
			{
				starting_pose = end_effector_state;
				saved_starting_pose=true;
			}

			// Reload everything except z to avoid drift
			double z_value = end_effector_state.translation().z();

//			old_end_effector.translation() << starting_pose.translation().x(), starting_pose.translation().y(), end_effector_state.translation().z();
//			old_end_effector.rotation() << starting_pose.rotation();
			/*
			Eigen::MatrixXd tmp(4,4);
			tmp << starting_pose.rotation().row(0), starting_pose.translation().x(),
				   starting_pose.rotation().row(1), starting_pose.translation().y(),
				   starting_pose.rotation().row(2), end_effector_state.translation().z(),
				   0,0,0,1;
			old_end_effector.matrix() = tmp;
			*/

			/*****Change new Cartesian coordinates*****/
			Eigen::Affine3d transform = Eigen::Affine3d::Identity();
			double o_x = 0.0;
			double o_y = 0.0;
			double o_z = 0.0;

//			if(roll>30)
//				o_x = -0.05;
//			if(roll<-30)
//				o_x = 0.05;
//			if(pitch>30)
//				o_y = -0.05;
//			if(pitch<-30)
//				o_y = 0.05;


			//		std::cout << "*****dx dy dz*****" << std::endl;
			//		std::cout << dx << " " << dy << " " << dz << std::endl;
			//transform.translation() << dx, dy, dz;.

			t_z = 0.05;
			z_max = 0.33;
			z_min = 0.15;

			t_z = up_down_state*t_z;

			if( z_value > z_max && t_z > 0)
				t_z = 0.0;

			if( z_value < z_min && t_z < 0)
				t_z = 0.0;

//			if( z_value > z_max || z_value < z_min)
//				t_z = 0.0;
//			else
//				t_z = .05;

			//std::cout << "z: " << z_value << ", tz: " << t_z << "\n";

			transform.translation() << 0.0, 0.0, t_z;

			Eigen::AngleAxisd rollAngle(  o_x, Eigen::Vector3d::UnitX() );
			Eigen::AngleAxisd pitchAngle( o_y, Eigen::Vector3d::UnitY() );
			Eigen::AngleAxisd yawAngle(   o_z, Eigen::Vector3d::UnitZ() );

			Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;
			Eigen::Matrix3d rotationMatrix = q.matrix();
			transform.rotate(rotationMatrix);

			//new_end_effector = transform * old_end_effector;
			new_end_effector = transform * end_effector_state;

			/***** Jacobian *****/
			Eigen::Vector3d reference_point_position(0.0,0.0,0.0);
			Eigen::MatrixXd jacobian;
			kinematic_state->getJacobian( joint_model_group, kinematic_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
					reference_point_position,
					jacobian);

			/***** Anaytical Jacobian *****/
			Eigen::MatrixXd T(3,3);
			Eigen::Vector3d ea = end_effector_state.rotation().eulerAngles(2, 1, 2);
			double a=ea(0);
			double b=ea(1);
			T << 0, -sin(a), cos(a)*sin(b),
			     0,  cos(a), sin(a)*cos(b),
				 1,       0,        cos(b);

			Eigen::MatrixXd A(6,6);
			A << Eigen::MatrixXd::Identity(3,3), Eigen::MatrixXd::Zero(3,3),
			     Eigen::MatrixXd::Zero(3,3),                              T;

			Eigen::MatrixXd Jana(6,5);
			Jana = A.inverse()*jacobian;

			/***** Inverse Jacobian *****/
			//Eigen::MatrixXd pinvJ = (Jana.transpose()*Jana).inverse()*Jana.transpose();
			Eigen::MatrixXd pinvJ = (jacobian.transpose()*jacobian).inverse()*jacobian.transpose();

			/***** Joint velocity *****/
			double alpha_gain = 1.5;
			Eigen::MatrixXd alpha = alpha_gain*Eigen::MatrixXd::Identity(5,5);

			Eigen::VectorXd x_cur(6);
			Eigen::VectorXd x_d(6);

			x_cur << end_effector_state.translation(), end_effector_state.rotation().eulerAngles(0, 1, 2); //, end_effector_state.rotation();
			x_d   << new_end_effector.translation(), new_end_effector.rotation().eulerAngles(0, 1, 2);

			Eigen::VectorXd joint_velocity = alpha*pinvJ*(x_d - x_cur);

			//		std::cout << "x_d" << std::endl << x_d << std::endl;
			//		std::cout << "x_cur" << std::endl << x_cur << std::endl;
			//		std::cout << "pseudoInverse_jacobian" << std::endl << pseudoInverse_jacobian << std::endl;
			//		std::cout << "alpha" << std::endl << alpha << std::endl;
			//      std::cout << "joint_velocity" << std::endl << joint_velocity << std::endl;

			/***** Publish *****/
			for(int i=0;i<5;i++)
			{
				command.velocities[i].value = joint_velocity(i);
			}
			up_down_state = 0;
			//std::cout <<  "Joint velocity: \n" << joint_velocity << "\n";

			velPub.publish(command);

			/**********Don't Delete. For future reference**********/
			//Computer SVD
			//Eigen::JacobiSVD<MatrixXd> svd(jacobian,Eigen::ComputeFullU|Eigen::ComputeFullV);
			//ROS_INFO_STREAM("U:\n" << svd.matrixU());
			//ROS_INFO_STREAM("Sigma:\n" << svd.singularValues());
			//ROS_INFO_STREAM("V:\n" << svd.matrixV());
			/**********Don't Delete. For future reference**********/

		}

		loop_rate.sleep();
	}

	ros::shutdown();
	return 0;
}
