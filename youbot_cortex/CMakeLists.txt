cmake_minimum_required(VERSION 2.8.3)
project(youbot_cortex)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  cortex_msgs
  brics_actuator
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

###################################
## catkin specific configuration ##
###################################
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES
#  CATKIN_DEPENDS roscpp std_msgs cortex_msgs brics_actuator
#  DEPENDS system_lib
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_executable(         youbot_cortex  src/youbot_cortex.cpp)
target_link_libraries(  youbot_cortex  ${catkin_LIBRARIES})
add_dependencies(       youbot_cortex  cortex_msgs_generate_messages_cpp)
