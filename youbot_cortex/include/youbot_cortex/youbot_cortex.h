#ifndef YOUBOT_CORTEX_H_
#define YOUBOT_CORTEX_H_

#include <ros/ros.h>

#include <brics_actuator/JointPositions.h>
#include <boost/units/systems/si.hpp>
#include <boost/units/io.hpp>

#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <cortex_msgs/SetBool.h>
#include <cortex_msgs/ControlType.h>

const char TabletModeSeperation[]   = " ,\t\n";

class YoubotCortex
{
	private:

		enum mode {OFF,individualJoint,combinedJoint,cartesian} controlSelected;
		enum joints {ARM_1,ARM_2,ARM_3,ARM_4,ARM_5} jointSelected;
		bool updatedControlSelected;

		mode prevControlSelected;

		//YouBotArm* arm;
		//BaseController* base;
		
		ros::NodeHandle nh;

		ros::Subscriber controlModeSub;
		ros::Subscriber tabletForceSub;
		ros::Subscriber tabletIRSub;
		ros::Subscriber tabletjointSub;
		ros::Subscriber tabletGripperSub;
		void controlModeCB(const std_msgs::Int8::ConstPtr& msg);
		void tabletJointCB(const std_msgs::String::ConstPtr& msg);
		void tabletForceCB(const std_msgs::Int8::ConstPtr& msg);
		void tabletIRCB(const std_msgs::Int8::ConstPtr& msg);
		void tabletGripperCB(const std_msgs::Int8::ConstPtr& msg);

		ros::ServiceClient force_srvClient;
		ros::ServiceClient ir_srvClient;
		ros::ServiceClient arm_srvClient;
		ros::ServiceClient cartesian_srvClient;
		ros::ServiceClient jointSelector_srvClient;

		ros::Publisher gripperPub;
		brics_actuator::JointPositions gripper_command;
		void gripper(bool open);

		cortex_msgs::SetBool srvsStatus;
		cortex_msgs::ControlType typeSrvMsg;

	public:

		void runOnce();
		YoubotCortex();
		~YoubotCortex();
		
		bool toggleForce(bool turnOn);
		bool toggleIR(bool turnOn);
		bool toggleArmController(YoubotCortex::mode m);
		bool toggleJointSelector(YoubotCortex::joints j);
};

#endif /*YOUBOT_CORTEX_H_*/
