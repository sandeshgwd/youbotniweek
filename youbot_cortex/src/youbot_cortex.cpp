#include <youbot_cortex/youbot_cortex.h>

YoubotCortex::YoubotCortex()
{
	//arm = new YouBotArm();
	//base = new BaseController();

	force_srvClient = nh.serviceClient<cortex_msgs::SetBool>("/controllers/force/base");
	ir_srvClient 	= nh.serviceClient<cortex_msgs::SetBool>("/controllers/ir/gripper");

	arm_srvClient 	= nh.serviceClient<cortex_msgs::ControlType>("/controllers/arm");
	jointSelector_srvClient	= nh.serviceClient<cortex_msgs::ControlType>("/controllers/arm/jointSelector");
	cartesian_srvClient = nh.serviceClient<cortex_msgs::SetBool>("/controllers/cartesian");

	controlModeSub = nh.subscribe("/tablet_mode",1,&YoubotCortex::controlModeCB,this);
	tabletjointSub = nh.subscribe("/joint_selector",1,&YoubotCortex::tabletJointCB,this);
	tabletForceSub = nh.subscribe("/tablet_force_desired",1,&YoubotCortex::tabletForceCB,this);
	tabletIRSub = nh.subscribe("/tablet_ir_desired",1,&YoubotCortex::tabletIRCB,this);
	tabletGripperSub = nh.subscribe("/tablet_gripper_desired",1,&YoubotCortex::tabletGripperCB,this);

	// Gripper
	gripperPub=nh.advertise<brics_actuator::JointPositions>("arm_1/gripper_controller/position_command",1);
	gripper_command.positions.resize(2);
	gripper_command.positions[0].joint_uri="gripper_finger_joint_l";
	gripper_command.positions[0].unit=boost::units::to_string(boost::units::si::meter);
	gripper_command.positions[1].joint_uri="gripper_finger_joint_r";
	gripper_command.positions[1].unit=boost::units::to_string(boost::units::si::meter);

	controlSelected = YoubotCortex::OFF;
	updatedControlSelected=false;
}

YoubotCortex::~YoubotCortex()
{
}

/*********************************** TABLET CB ***********************************/

void YoubotCortex::controlModeCB(const std_msgs::Int8::ConstPtr& msg)
{
	YoubotCortex::mode tmp = (YoubotCortex::mode)msg->data;

	if(controlSelected != tmp)
	{
		prevControlSelected=controlSelected;
		controlSelected = tmp;
		std::cout<<"Control mode: "<<(int)controlSelected<<"\n";
		toggleArmController(controlSelected);
	}

}

void YoubotCortex::tabletJointCB(const std_msgs::String::ConstPtr& msg)
{
	std_msgs::String in = *msg;
	char* token = strtok(const_cast<char*>(in.data.c_str()),TabletModeSeperation);

	for(int i=0;i<5;i++)
	{
		if(atof(token)==1)
		{
			YoubotCortex::joints tmp = (YoubotCortex::joints)i;
			if(jointSelected != tmp)
			{
				jointSelected = tmp;
				std::cout<<"Selected joint: "<<(int)jointSelected<<"\n";
				toggleJointSelector(jointSelected);
			}
			return;
		}
		token = strtok(NULL,TabletModeSeperation);
	}
}

void YoubotCortex::tabletForceCB(const std_msgs::Int8::ConstPtr& msg)
{
	if(msg->data==1)
	{
		toggleForce(true);
	}
	else
	{
		toggleForce(false);
	}
}

void YoubotCortex::tabletIRCB(const std_msgs::Int8::ConstPtr& msg)
{
	if(msg->data==1)
	{
		toggleIR(true);
	}
	else
	{
		toggleIR(false);
	}
}

void YoubotCortex::tabletGripperCB(const std_msgs::Int8::ConstPtr& msg)
{
	if(msg->data==1)
	{
		gripper(true);
	}
	else
	{
		gripper(false);
	}
}

/*********************************** Services ***********************************/

bool YoubotCortex::toggleArmController(YoubotCortex::mode m)
{

	// Stop previous controller
	if(prevControlSelected==YoubotCortex::cartesian)
	{
		srvsStatus.request.variable = false;
		cartesian_srvClient.call(srvsStatus);
	}
	else
	{
		typeSrvMsg.request.type = YoubotCortex::OFF;
		arm_srvClient.call(typeSrvMsg);
	}

	// Start new controller
	if(m==YoubotCortex::cartesian)
	{
		srvsStatus.request.variable = true;
		cartesian_srvClient.call(srvsStatus);
	}
	else
	{
		typeSrvMsg.request.type = m;
		arm_srvClient.call(typeSrvMsg);
	}
}

bool YoubotCortex::toggleJointSelector(YoubotCortex::joints j)
{
	typeSrvMsg.request.type = j;
	if (jointSelector_srvClient.call(typeSrvMsg))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool YoubotCortex::toggleForce(bool turnOn)
{
	srvsStatus.request.variable = turnOn;
	  if (force_srvClient.call(srvsStatus))
	  {
	    ROS_INFO("Cortex: force-base controller is %i",turnOn);
	    return true;
	  }
	  else
	  {
	    ROS_ERROR("Cortex: Failed to turn on force-base controller!");
	    return false;
	  }
}

bool YoubotCortex::toggleIR(bool turnOn)
{
	srvsStatus.request.variable = turnOn;
	  if (ir_srvClient.call(srvsStatus))
	  {
	    ROS_INFO("Cortex: ir-gripper controller is %i",turnOn);
	    return true;
	  }
	  else
	  {
	    ROS_ERROR("Cortex: Failed to turn on ir-gripper controller!");
	    return false;
	  }
}
/*********************************** Gripper ***********************************/
void YoubotCortex::gripper(bool open)
{
	if(open)
	{
		std::cout << "Cortex: opening gripper" << std::endl;
		gripper_command.positions[0].value=0.011;
		gripper_command.positions[1].value=0.011;
	}
	else
	{
		std::cout << "Cortex: closing gripper" << std::endl;
		gripper_command.positions[0].value=0.0;
		gripper_command.positions[1].value=0.0;
	}
	gripperPub.publish(gripper_command);
}

/*********************************** MAIN ***********************************/


int main(int argc, char** argv)
{
	ros::init(argc,argv,"youbot_cortex");
	ros::NodeHandle n;

	YoubotCortex cortex;

//	int rate = 50;
//	ros::Rate r(rate);
//
//	while(ros::ok())
//	{
//		ros::spinOnce();
//		r.sleep();
//	}

	ros::spin();

	return 0;
}
