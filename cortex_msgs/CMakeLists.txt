cmake_minimum_required(VERSION 2.8.3)
project(cortex_msgs)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  std_srvs
  message_generation
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Generate messages in the 'msg' folder
# add_message_files(
#   FILES
#   Message1.msg
#   Message2.msg
# )

## Generate services in the 'srv' folder
add_service_files(
   FILES
   SetBool.srv
   ControlType.srv
)

## Generate added messages and services with any dependencies listed here
generate_messages(
   DEPENDENCIES
   std_msgs
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES cortex_msgs
  CATKIN_DEPENDS std_msgs std_srvs message_runtime
#  DEPENDS system_lib
)