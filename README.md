# Install
```
#git clone https://github.com/youbot/youbot_description.git
git clone https://github.com/youbot/youbot_driver.git --branch hydro-devel
git clone https://github.com/svenschneider/youbot-manipulation.git
```

# Running
## Step 1
Start the drivers on the youbot
```
ssh youbot@192.168.1.5
sudo -s
roslaunch youbot_cortex start.launch
```
and
```
ssh youbot@192.168.1.5
rosrun apc_vacuum serial_read
```
If starting up for the first time do
```
ssh youbot@192.168.1.5
sudo chmod a+rw /dev/ttyACM0
```
## Step 2
Start tablet app and connect to the ROS master.
## Step 3
On desktop or youbot:
```
roslaunch youbot_cortex run.launch
```
 or
```
roslaunch youbot_cortex youbot_cortex.launch
roslaunch youbot_cartesian youbot_cartesian.launch
```