/*
 * ircontrol.h
 *
 *  Created on: Jul 28, 2015
 *      Author: rommel
 */

#ifndef SOURCE_DIRECTORY__YOUBOT_CORTEX_SRC_IRCONTROL_H_
#define SOURCE_DIRECTORY__YOUBOT_CORTEX_SRC_IRCONTROL_H_

#include <std_msgs/String.h>
#include <iostream>
#include <string>
#include <sstream>

class IRcontrol {
public:

	IRcontrol();
	virtual ~IRcontrol();
private:
	ros::NodeHandle nh;
	ros::Subscriber getSerialDataSub;
	double ir_sensor1[4][4];
	double ir_sensor2[4][4];
	double ir_sensor3[4][4];
	int i;
	double average;
	double previousAverage;
	double gripperPos;
	double gripperThreshold;
	double resetIrGripperThreshold;
	bool publish_ir_gripper;
	ros::Publisher gripperPositionPublisher;
	std::vector <brics_actuator::JointValue> gripperJointPositions;//message types
	brics_actuator::JointPositions command;
};

#endif /* SOURCE_DIRECTORY__YOUBOT_CORTEX_SRC_IRCONTROL_H_ */
