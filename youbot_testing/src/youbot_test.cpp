#include <apc_baxter/youbot_test.h>
#include <ros/console.h>//truce

YouBotTest::YouBotTest(ros::NodeHandle nh):nh_(nh)
{
	// Subscribers
  sub_joint_selector_ = nh_.subscribe("joint_selector", 1, &YouBotTest::joint_selectorCB,this);
  sub_tablet_orientation_ = nh_.subscribe("arm_vel", 1, &YouBotTest::tablet_orientationCB,this);
  sub_tablet_mode_ = nh_.subscribe("tablet_mode", 1, &YouBotTest::tabletModeCB,this);

  // Publishers
  arm_pub_ = nh.advertise<brics_actuator::JointPositions>("/arm_1/arm_controller/position_command",1);
  gripper_pub_ = nh.advertise<brics_actuator::JointPositions>("/arm_1/gripper_controller/position_command",1);
  
  //Message Construction
  armPositions.resize(5);
  
  armPositions[0].joint_uri  = "arm_joint_1";
  armPositions[1].joint_uri  = "arm_joint_2";
  armPositions[2].joint_uri  = "arm_joint_3";
  armPositions[3].joint_uri  = "arm_joint_4";
  armPositions[4].joint_uri  = "arm_joint_5";

  armPositions[0].unit = boost::units::to_string(boost::units::si::radians);
  armPositions[1].unit = boost::units::to_string(boost::units::si::radians);
  armPositions[2].unit = boost::units::to_string(boost::units::si::radians);
  armPositions[3].unit = boost::units::to_string(boost::units::si::radians);
  armPositions[4].unit = boost::units::to_string(boost::units::si::radians);

}

void YouBotTest::tabletModeCB(std_msgs::Int8 msg){
	mode_selected = msg.data;
	//std::cout << "Mode Selected: " << mode_selected << std::endl;
}

void YouBotTest::joint_selectorCB(const std_msgs::String::ConstPtr& msg)
{
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
  std_msgs::String in = *msg;
  char* token = strtok(const_cast<char*>(in.data.c_str()),seps);

  for(int i=0;i<5;i++)
  {
	  joint_selected[i] = atof(token);
	  token = strtok(NULL,seps);
  }
}

void YouBotTest::tablet_orientationCB(const geometry_msgs::Twist::ConstPtr& msg)
{
	tablet_orientation = *msg;

	tablet_yaw = tablet_orientation.linear.x;
	tablet_pitch = tablet_orientation.linear.y;
	tablet_roll = tablet_orientation.angular.z;
	//ROS_INFO("got here");
}


/*
 * Goal: Using moveit and the fact that there are no blocking functions 
 * we compute a cartesian path that is converted into joint velocities. 
 * The key is that when computing the cartesian path, we set the end 
 * velocities to non-zero so that every joint velocity doesn't slow down
 * until required.
 * 
 * Ctrl-C to exit program
 * 
 */
void YouBotTest::cartesian_joint_velocity_test()
{
	//Temp
	geometry_msgs::Pose target_pose_;
	
	//Path paramters
	double dx = 0.01; //
	std::vector<geometry_msgs::Pose> waypoints;
	double fractionThreshold;
	//youbot_.moveToPose(BaxterMoveit::RIGHT,"candle",true);
	//ros::Duration(1.0).sleep();
	//youbot_.moveToPose(BaxterMoveit::RIGHT,"folded",false);
	//ros::Duration(1.0).sleep();
	//youbot_.moveToPose(BaxterMoveit::RIGHT,"candle",false);
	//youbot_.moveToPose(BaxterMoveit::RIGHT,"candle_mod",false);
	//ros::Duration(1.0).sleep();
	//~ ros::Duration(5.0).sleep();
	
	ROS_WARN("The candle position achieved!");
	
		
	geometry_msgs::Pose current_pose;//truce
	youbot_.getEndeffectorPose(BaxterMoveit::RIGHT, &current_pose); //truce
  
    
  target_pose_=current_pose;//truce, before giving delta increment/decrement
  
  	
	target_pose_.position.x    += 0.30; //truce
 	target_pose_.position.z    += 0.0; //truce
	target_pose_.position.y    += 0.0; //earlier 0.0
	waypoints.push_back(target_pose_);  //target set
		 
  //~ target_pose_.orientation.x    = 0.0;
  //~ target_pose_.orientation.y    = 0.0;
  //~ target_pose_.orientation.z    = 0.0;
  //~ target_pose_.orientation.w    = 1.0;
  
  //waypoints.push_back(target_pose_);  //
  
  //youbot_.executeCartePath(BaxterMoveit::RIGHT,waypoints,dx,fractionThreshold);
  
    
  target_pose_.position.x    -= 0.3; //truce
 	target_pose_.position.z    -= 0.0; //truce
	target_pose_.position.y    -= 0.0; //
	
	//~ target_pose_.orientation.x    = 0.0;
  //~ target_pose_.orientation.y    = 0.0;
  //~ target_pose_.orientation.z    = 0.0;
  //~ target_pose_.orientation.w    = 1.0;
	
	waypoints.push_back(target_pose_);  //
  youbot_.executeCartePath(BaxterMoveit::RIGHT,waypoints,dx,fractionThreshold);
   	
    
  //TODO add the velocity as a parameter
  
  return;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "youbot_testing_node",ros::init_options::AnonymousName);
	
	ros::NodeHandle nh_;
	YouBotTest youbot_test_(nh_);
	
	int rate = 10;
	ros::Rate r_(rate);
	
	//TODO: Move to a home position
	
	//Path paramters
	
	youbot_test_.cartesian_joint_velocity_test();
	
	/**while(ros::ok())
	{
		ros::spinOnce();
		
		youbot_test_.cartesian_joint_velocity_test();
		
		r_.sleep();
	}*/
	
	return 0;
}

