#include <ros/ros.h>

#include <iostream>
#include <string>
#include <Eigen/Dense>
#include <Eigen/SVD>

// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

#include <brics_actuator/JointVelocities.h>
#include <brics_actuator/JointValue.h>
#include <brics_actuator/JointPositions.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>

#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/conversion.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/plane_angle.hpp>
#include <boost/units/systems/si/angular_velocity.hpp>

using Eigen::MatrixXd;

Eigen::VectorXd velocity;
ros::Publisher velPub;
ros::Publisher armPosPub;
ros::Subscriber jointSub;
ros::Subscriber tabletRotSub;
ros::Subscriber tabletZSub;
bool receivedJointValues;
static double VEL[] = {2.94961, 1.352, -2.591, 0.1, 0.1};
static const std::string JOINTNAME_PRE = "arm_joint_";
static const uint NUM_ARM_JOINTS = 5;
sensor_msgs::JointState::Ptr joint_state;
std::vector<std::string> joint_names;
Eigen::VectorXd reference_velocity(6);

bool yaw_range_defined;

double* joint_values;

double roll;
double pitch;
double yaw;
double arm_displacement;
double gripper_displacement;
double platter_displacement;
double* joint_gains;

double* min_joint_values;
double* max_joint_values;
double* max_joint_displacement_from_candle;
double min_yaw;
double max_yaw;
bool max_inverted;
bool min_inverted;
bool received_new_tablet_orientation;
double center;
double range;

void printJointPosition(robot_state::RobotStatePtr &kinematic_state,const std::string* joint_names){
	const double* joint_values = kinematic_state->getJointPositions(*joint_names);

	//ROS_INFO_STREAM("Joint Names:" << joint_names);
}

void joint_state_CB(const sensor_msgs::JointState::Ptr& msg)
{
	//	std::cout << "joint_state_CB" << std::endl;

	if(msg->name[0] == "arm_joint_1")
	{
		for(int i=0;i<5;i++){
			joint_values[i] = msg->position[i];
		}
		receivedJointValues = true;
	}
}

void define_yaw_bounds(double center_t)
{
	//Range is + or -30

	center = center_t;
	//center=center+360;
	min_yaw = center-range;
	max_yaw = center+range;

	if(max_yaw>360)
	{
		max_inverted=true;
		max_yaw-=360;
		//std::cout << "Max Inverted" << std::endl;
	}
	if(min_yaw<0)
	{
		min_inverted=true;
		min_yaw+=360;
		//std::cout << "Min Inverted" << std::endl;
	}

	yaw_range_defined = true;

	return;
}

double bounded_yaw_value_range(double value)
{
	//std::cout << "Value:" << value << " Min:" << min_yaw << " Max:" << max_yaw << std::endl;

	if(max_inverted || min_inverted)
	{

		if(value>min_yaw&&value<360 || value>0&&value<max_yaw){
			//std::cout << "In range:" << value << std::endl;
		}
		if(!(value>min_yaw&&value<360) && value>180)
		{
			value = min_yaw;
		}
		else if(!(value>0&&value<max_yaw) && value<180)
		{
			value = max_yaw;
		}
		//std::cout << "Value:" << value << std::endl;
	}
	else{
		if(value<min_yaw){
			value = min_yaw;
		}
		else if(value>max_yaw){
			value = max_yaw;
		}else{
			//std::cout << "In range:" << value << std::endl;
		}

	}
	return value;
}

void find_yaw_displacement_distance()
{
	if((abs(360-center) <= range) && (abs(yaw) <= range))
	{
		//std::cout << "Case 1" << std::endl;
		platter_displacement = yaw + 360 - center;
	}else if((abs(center)<=range) && abs(360-yaw)<=range)
	{
		//std::cout << "Case 2" << std::endl;
		platter_displacement = -(center+360-yaw);
	}else{
		//std::cout << "Case 3" << std::endl;
		platter_displacement = yaw-center;
	}

	std::cout << "Displacement:" << platter_displacement << std::endl;
}

void tablet_orientation_CB(const geometry_msgs::Twist::Ptr& msg)
{
	yaw 	= msg->linear.x;		// 0 -> 360
	pitch 	= msg->linear.y;		// -180 -> 180 (up is negative)
	roll	= msg->angular.z;		// -90 -> 90 (clockwise is negative)

	if(abs(yaw)<30)
	{
		yaw = 0;
	}

	if(abs(pitch)<30)
	{
		pitch = 0;
	}

	if(abs(roll)<30)
	{
		roll = 0;
	}

	arm_displacement = (pitch/3)*M_PI/180; //Divides displacement by 3 and converts to radians
	gripper_displacement = (roll/3)*M_PI/180;
	//platter_displacement = (yaw/3)*M_PI/180;

	if(!yaw_range_defined)define_yaw_bounds(yaw);
	else
	{
		yaw = bounded_yaw_value_range(yaw);
	}

	yaw = 0;
	received_new_tablet_orientation = true;
	//std::cout << "Pitch to arm:" << arm_displacement << std::endl;
}

double boundedJointValue(double proposed_value,int index,double& result)
{
	if(proposed_value<min_joint_values[index])
	{
		result = min_joint_values[index];
	}
	if(proposed_value>max_joint_values[index])
	{
		result = max_joint_values[index];
	}
}

double boundedPostionCommand(brics_actuator::JointPositions& position_command)
{
	for(int i=0;i<5;i++)
	{
		boundedJointValue(position_command.positions[i].value,i,position_command.positions[i].value);
	}
}

void move_arm(brics_actuator::JointPositions& position_command)
{
	position_command.positions[1].value += (joint_gains[1]*arm_displacement);
	position_command.positions[2].value += (joint_gains[2]*arm_displacement);
	position_command.positions[3].value += (joint_gains[3]*arm_displacement);

	boundedPostionCommand(position_command);
	//armPosPub.publish(position_command);
}

void rotate_gripper(brics_actuator::JointPositions& position_command)
{
	position_command.positions[4].value += (joint_gains[4]*gripper_displacement);
	boundedPostionCommand(position_command);
	//armPosPub.publish(position_command);
}

void rotate_base(brics_actuator::JointPositions& position_command)
{
	find_yaw_displacement_distance();
	platter_displacement = platter_displacement*(M_PI/180);
	//position_command.positions[0].value += (joint_gains[0]*platter_displacement);
}

int main(int argc, char** argv)
{
	//Initialize ROS
	std::cout << "Init" << std::endl;
	ros::init (argc, argv, "moveit_getJacobian");
	ros::NodeHandle n;
	ros::AsyncSpinner spinner(1);
	spinner.start();

	/**********Variable initialization**********/
	yaw_range_defined = false;
	min_inverted=false;
	max_inverted=false;
	min_joint_values = new double[5];
	max_joint_values = new double[5];
	joint_gains = new double[5];
	range = 30;

	/**********ROS Publisher**********/
	armPosPub = n.advertise<brics_actuator::JointPositions>("/arm_1/arm_controller/position_command", 1);
	velPub = n.advertise<brics_actuator::JointVelocities>("arm_1/arm_controller/velocity_command", 1);
	jointSub = n.subscribe("/joint_states", 1, joint_state_CB);
	tabletRotSub = n.subscribe("/arm_vel", 1, tablet_orientation_CB);
	//sub_3 = n.subscribe("/joystick_pose", 1, joystick_pose_CB);

	/**********Joint max/min values**********/
	std::cout << "Init Min/Max Vector" << std::endl;
	min_joint_values[0] = 0.0100693;
	min_joint_values[1] = 0.49062;
	min_joint_values[2] = -3.26034;
	min_joint_values[3] = 0.77665;
	min_joint_values[4] = 0.110620;

	max_joint_values[0] = 5.84013;
	max_joint_values[1] = 2.09893;
	max_joint_values[2] = -1.602165;
	max_joint_values[3] = 2.43484;
	max_joint_values[4] = 5.6415;

	/**********Joint max/min values**********/
	std::cout << "Init Joint Gains Vector" << std::endl;
	joint_gains[0] = 0.005;
	joint_gains[1] = 0.05;
	joint_gains[2] = 0.05;
	joint_gains[3] = 0.05;
	joint_gains[4] = 0.05;

	/**********Set loop rate**********/
	ros::Rate loop_rate(30);

	/**********Init command vector**********/
	std::cout << "Init command vector" << std::endl;
	brics_actuator::JointVelocities command;
	command.velocities.resize(5);
	for(int i=0;i<5;i++)
	{
		command.velocities[i].joint_uri = "arm_joint_" + boost::lexical_cast<std::string>(i+1);
		command.velocities[i].value = 0;
		command.velocities[i].unit = boost::units::to_string(boost::units::si::radians_per_second);
	}

	/**********Init command position**********/
	brics_actuator::JointPositions position_command;
	std::vector <brics_actuator::JointValue> armJointPositions;
	armJointPositions.resize(5);
	std::stringstream jointName;
	for (int i = 0; i < 5; ++i) {
		jointName.str("");
		jointName << "arm_joint_" << (i + 1); // Assamble "arm_joint_1", "arm_joint_2", ...
		armJointPositions[i].joint_uri = jointName.str();
		armJointPositions[i].value = 0; // Here we set the new value.
		armJointPositions[i].unit = boost::units::to_string(boost::units::si::radians); // Set unit.
	};

	/**********Get joint values*************/
	receivedJointValues = false;
	joint_values = new double[5];
	while(!yaw_range_defined)
	{
		ros::spinOnce();
		loop_rate.sleep();
	}

	/******Move to candle position******/
	armJointPositions[0].value = 2.9496;
	armJointPositions[1].value = 1.1344;
	armJointPositions[2].value = -2.5482;
	armJointPositions[3].value = 1.789;
	armJointPositions[4].value = 2.9234;

	/**********Publish**********/
	//Arm Position:Candle
	position_command.positions = armJointPositions;
	armPosPub.publish(position_command);
	armPosPub.publish(position_command);
	armPosPub.publish(position_command);
	ros::Duration(3.0).sleep();
	// Main loop
	ROS_INFO("Starting main loop");
		while(ros::ok())
		{
			ros::spinOnce();
			//find_yaw_displacement_distance();

			move_arm(position_command);
			rotate_base(position_command);
			rotate_gripper(position_command);


			/**********Publish**********/
			if(received_new_tablet_orientation){
				armPosPub.publish(position_command);
				received_new_tablet_orientation = false;
			}

			loop_rate.sleep();
		}

	std::cout << "Ending main" << std::endl;
	ros::shutdown();
	return 0;
}


