/*
 * ircontrol.cpp
 *
 *  Created on: Jul 28, 2015
 *      Author: rommel
 */

#include "ircontrol.h"

IRcontrol::IRcontrol() {
	// TODO Auto-generated constructor stub
	getSerialDataSub = nh.subscribe("/serial",1,&YouBotArm::getForceDataCB,this);
}

IRcontrol::~IRcontrol() {
	// TODO Auto-generated destructor stub
}

void IRcontrol::irGripperControl()
{

	std::cout << "irGripperControl: Begin" <<std::endl;
	for (i=0;i<4;i++)
	{
		average=ir_sensor1[i][0]+ir_sensor1[i][1]+ir_sensor1[i][2]+ir_sensor1[i][3];
	}

	average = average/16;

	std::cout << "irGripperControl: Calculate Average" <<std::endl;
	std::cout << "Average: " << average << std::endl;

	std::cout << "irGripperControl:Compare Average to threshold" <<std::endl;
	if(average<resetIrGripperThreshold)
	{
		publish_ir_gripper = true;
	}

	if (average>gripperThreshold)
	{
		if (gripperPos)
		{
			std::cout << "Closing Gripper" << std::endl;
			gripperJointPositions[0].joint_uri="gripper_finger_joint_l";
			gripperJointPositions[0].value=0;
			gripperJointPositions[0].unit=boost::units::to_string(boost::units::si::meter);
			gripperJointPositions[1].joint_uri="gripper_finger_joint_r";
			gripperJointPositions[1].value=0;
			gripperJointPositions[1].unit=boost::units::to_string(boost::units::si::meter);

			gripperPos = false;
			previousAverage=average;
		}
		else
		{
			std::cout << "Opening Gripper" << std::endl;
			gripperJointPositions[0].joint_uri="gripper_finger_joint_l";
			gripperJointPositions[0].value=1;
			gripperJointPositions[0].unit=boost::units::to_string(boost::units::si::meter);
			gripperJointPositions[1].joint_uri="gripper_finger_joint_r";
			gripperJointPositions[1].value=1;
			gripperJointPositions[1].unit=boost::units::to_string(boost::units::si::meter);

			gripperPos = true;
			previousAverage=average;
		}
	}

	std::cout << "irGripperControl: Publish" <<std::endl;
	if(publish_ir_gripper)
	{
		command.positions=gripperJointPositions;
		//gripperPositionPublisher.publish(command);
		publish_ir_gripper = false;
	}


}

