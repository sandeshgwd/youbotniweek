/*
 * force_base_controller.cpp
 *
 *  Created on: Jul 30, 2015
 *      Author: Sven Cremer
 *  Editted on: Sept 25, 2015 Sandesh Gowda Custom sensor integration
 */

#include <youbot_force_sensors/force_base_controller.h>

ForceBaseController::ForceBaseController()
{
	vDiv = 15.0;
	forceThreshold = 2.5;
	rate = 0.02;

	dataSub = nh.subscribe("force_sensor_data",1, &ForceBaseController::dataCB, this);
	cmd_vel = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
	toggle_srv = nh.advertiseService("/controllers/force/base", &ForceBaseController::toggle_srv_CB, this);

	cmdTimer = nh.createTimer(ros::Duration(rate), &ForceBaseController::timerCB, this);

	controllerEnabled = false;
	sentStopCommand = false;
	
  initialAverageProcess = true;

	sensorOFF = true;
	sensorON = false;
	sensorReturnToOFF=false;
	sensorRising = false;

	ref_force_3    = 0.0;
	applied_force_3    = 0.0;
	prev_applied_force_3 = 0.0;

	change_force_3    = 0.0;
	force_3_threshold = 0.05;
	force_counter = 0;
	for(int i = 0; i <WINDOW_SIZE; i++)
	{
		ref_force_3_window[i] = 0.0;
		applied_force_3_window[i] = 0.0;
	}
}

ForceBaseController::~ForceBaseController()
{
}

bool ForceBaseController::toggle_srv_CB(cortex_msgs::SetBool::Request &req, cortex_msgs::SetBool::Response &res)	// TODO change message type
{
	if(!req.variable)
	{
		controllerEnabled = false;
		ROS_INFO("DISABLED force base controller!");
	}
	else
	{
		controllerEnabled = true;
		ROS_INFO("ENABLED force base controller!");
	}
	return true;
}

void ForceBaseController::dataCB(const apc_vacuum::forceSensor::ConstPtr& msg)
{

	// Check size
	if(msg->data.size() != 4)
	{
		ROS_ERROR("ForceBaseController::dataCB - received data has wrong size");
		return;
	}

	force_1 = msg->data[0];
	force_2 = msg->data[1];
	force_3 = msg->data[2];
	force_4 = msg->data[3];
	
	//moving average filter for single sensor at force_3 module
  if(initialAverageProcess){
		ref_force_3_window[force_counter] = force_3;	
		applied_force_3_window[force_counter] = force_3;	
		ref_force_3 += force_3;
		applied_force_3 += force_3;
		force_counter++;
		if(force_counter == WINDOW_SIZE){
			initialAverageProcess = false;
			ref_force_3 = ref_force_3/WINDOW_SIZE;
			applied_force_3 = applied_force_3/WINDOW_SIZE;
			prev_applied_force_3 = applied_force_3;
		}
	}
  else {
		// calc applied force
		force_counter = force_counter % WINDOW_SIZE;
		applied_force_3 = applied_force_3 + (force_3/WINDOW_SIZE) - (applied_force_3_window[force_counter]/WINDOW_SIZE);
		applied_force_3_window[force_counter] = force_3;
		force_counter++;
		// calc reference force
		if(std::fabs(ref_force_3 - force_3) < 0.05){
			ref_force_3 = ref_force_3 + (force_3/WINDOW_SIZE) - (ref_force_3_window[force_counter]/WINDOW_SIZE);
			ref_force_3_window[force_counter] = force_3;
		}
		else
			ref_force_3_window[force_counter] = ref_force_3;
	}
}

void ForceBaseController::publishCmdVel()
{
	geometry_msgs::Twist twist;

	twist.linear.x = ( tfForce1[0] + tfForce3[0] + tfForce2[0] + tfForce4[0] ) / vDiv;
	twist.linear.y = ( tfForce1[1] + tfForce3[1] + tfForce2[1] + tfForce4[1] ) / vDiv;

//	std::cout << "v_x: " << twist.linear.x << std::endl;
//	std::cout << "v_y: " << twist.linear.y << std::endl;

	cmd_vel.publish(twist);

	sentStopCommand=false;
}

void ForceBaseController::timerCB(const ros::TimerEvent&)
{

	if(!controllerEnabled)
		return;

	/*** Apply threshold ***/

	if(force_1<forceThreshold){
		force_1=0;
	}
	if(force_2<forceThreshold){
		force_2=0;
	}
	// for custom single sensor (strain gauge)

/* Check sensor region */
	/*if(sensorOFF)
	{
		// Check for first threshold
		if(std::fabs(applied_force_3 - ref_force_3) > force_3_threshold)
		{
			sensorOFF = false;
			sensorON = true;
			sensorRising = true;
		}
	}
	if(sensorON)
	{
		// Check for second threshold
		if( ( std::fabs(applied_force_3 - ref_force_3) < (force_3_threshold*2) ) && sensorRising == false)
		{
			sensorOFF = false;
			sensorON = false;
			sensorReturnToOFF=true;
		}
		else if( ( std::fabs(applied_force_3 - ref_force_3) > (force_3_threshold*2) ) && sensorRising )
		{
			sensorRising = false;
		}
	}

	if(sensorReturnToOFF)
	{
		// Check for first threshold
		if(std::fabs(applied_force_3 - ref_force_3) < force_3_threshold)
		{
			sensorOFF = true;
			sensorON = false;
			sensorReturnToOFF=false;
		}
	}
	std::cout<<"Flags :"<<sensorOFF << " , " << sensorON << " , " << sensorReturnToOFF <<std::endl;*/
/* Do math depending on region*/
	/*if(sensorOFF || sensorReturnToOFF)
	{
		change_force_3 = 0.0;
	}
	if(sensorON)
	{
		change_force_3 = (std::fabs(ref_force_3 - applied_force_3))*20;
	}*/
	//Assumption that the voltage drops when the sensor is pressed.
	if((ref_force_3 - applied_force_3) < force_3_threshold){
		change_force_3 = 0;
	}
 	else if((ref_force_3 - applied_force_3) > force_3_threshold){
		/*sensorON = true;
		if(prev_applied_force_3 == applied_force_3){
			sensorON = false;
		}
		if(sensorON)
		{*/
			change_force_3 = (std::fabs(ref_force_3 - applied_force_3))*20; // for linear velocity
		//}
	}
	prev_applied_force_3 = applied_force_3;
	if(force_4<forceThreshold){
		force_4=0;
	}
	std::cout << "F = {" << force_1 << " " << force_2 << " " << change_force_3 << " " << force_4 << "}\n";

	/*** Get transform ***/
	tf::StampedTransform lk2base_transform;
	try{
		listener.lookupTransform("/base_footprint", "/arm_link_5",
				ros::Time(0), lk2base_transform);
	}
	catch (tf::TransformException ex){
		ROS_ERROR("%s",ex.what());
		return;
	}

	/*** Transform forces into base frame ***/
	/***Sensor 1***/
	KDL::Vector local_force_1(0, force_1, 0);						// Depends on where the force sensor is placed
	KDL::Rotation rot;

	tf::quaternionTFToKDL(lk2base_transform.getRotation(),rot);		// generates the rotation matrix from link to base
	tfForce1 = (rot*local_force_1);									// transforms the force to the base frame

	/***Sensor 2***/
	KDL::Vector local_force_2(force_2, 0, 0);
	tf::quaternionTFToKDL(lk2base_transform.getRotation(),rot);
	tfForce2 = -(rot*local_force_2);

	/***Sensor 3***/
	KDL::Vector local_force_3(change_force_3,0, 0);
	tf::quaternionTFToKDL(lk2base_transform.getRotation(),rot);
	tfForce3 = (rot*local_force_3);

	/***Sensor 4***/
	KDL::Vector local_force_4(0, force_4, 0);
	tf::quaternionTFToKDL(lk2base_transform.getRotation(),rot);
	tfForce4 = -(rot*local_force_4);


	/*** Send base commands ***/
	if(force_4==0.0 && change_force_3==0.0 && force_2==0.0 && force_1==0.0)
	{
		if(!sentStopCommand)
		{
			geometry_msgs::Twist twist;
			cmd_vel.publish(twist);
			sentStopCommand=true;
		}
	}
	else
	{
		publishCmdVel();
	}

	return;
}



int main(int argc, char** argv)
{
	ros::init(argc,argv,"force_base_controller");
	ros::NodeHandle nh;

	ForceBaseController ctrl;

	ros::spin();

	return 0;
}


