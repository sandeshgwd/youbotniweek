/*
 * force_base_controller.h
 *
 *  Created on: Jul 30, 2015
 *      Author: Sven Cremer
 */

#ifndef YOUBOT_FORCE_BASE_CONTROLLER_H_
#define YOUBOT_FORCE_BASE_CONTROLLER_H_

#include <ros/ros.h>

#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>
#include <std_srvs/Empty.h>
#include <apc_vacuum/forceSensor.h>
#include <cortex_msgs/SetBool.h>

#include <tf/transform_listener.h>
#include <tf_conversions/tf_kdl.h>
#include <kdl/chain.hpp>

#define WINDOW_SIZE 20

class ForceBaseController
{
	private:

		ros::NodeHandle nh;

		ros::Timer cmdTimer;
		ros::Subscriber dataSub;
		ros::Publisher cmd_vel;
		ros::ServiceServer toggle_srv;

		bool toggle_srv_CB(cortex_msgs::SetBool::Request &req, cortex_msgs::SetBool::Response &res);
		void dataCB(const apc_vacuum::forceSensor::ConstPtr& msg);
		void timerCB(const ros::TimerEvent&);

		double rate;
		tf::TransformListener listener;

		double forceThreshold;
		double vDiv;
		double force_1, force_2, force_3, force_4;					// Received force data
		KDL::Vector tfForce1, tfForce2, tfForce3, tfForce4;			// Transformed force vectors

		void publishCmdVel();
		bool controllerEnabled;
		bool sentStopCommand;

		char initialAverageProcess;
		double ref_force_3;
		double applied_force_3;
		double prev_applied_force_3;

		double change_force_3;
		double force_3_threshold;
		double ref_force_3_window[WINDOW_SIZE];
		double applied_force_3_window[WINDOW_SIZE];

		int force_counter;

		char sensorOFF;
		char sensorON;
		char sensorReturnToOFF;
		char sensorRising;

	public:

		ForceBaseController();
		~ForceBaseController();

};


#endif /* YOUBOT_FORCE_BASE_CONTROLLER_H_ */
