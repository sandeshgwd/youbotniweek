/*
 * ir_gripper_controller.cpp
 *
 *  Created on: Jul 30, 2015
 *      Author: Sven Cremer
 */

#include <youbot_ir_sensors/ir_gripper_controller.h>

IrGripperController::IrGripperController()
{

	jointSub=nh.subscribe("/joint_states", 1, &IrGripperController::jointStateCB,this);
	dataSub = nh.subscribe("IR_sensor_data",1, &IrGripperController::dataCB, this);

	gripperPub=nh.advertise<brics_actuator::JointPositions>("arm_1/gripper_controller/position_command",1);
	toggle_srv = nh.advertiseService("/controllers/ir/gripper", &IrGripperController::toggle_srv_CB, this);

	statusPub1=nh.advertise<std_msgs::Float64>("/controllers/ir/status/SMA",1);
	statusPub2=nh.advertise<std_msgs::Float64>("/controllers/ir/status/AVG",1);

	// Gripper message
	gripper_command.positions.resize(2);
	gripper_command.positions[0].joint_uri="gripper_finger_joint_l";
	gripper_command.positions[0].unit=boost::units::to_string(boost::units::si::meter);
	gripper_command.positions[1].joint_uri="gripper_finger_joint_r";
	gripper_command.positions[1].unit=boost::units::to_string(boost::units::si::meter);

	// Sensor data
	window_size = 5;
	ir_values.resize(window_size);
	for(int i=0;i<window_size;i++)
		ir_values[i]=0;
	ir_values_indx = 0;
	SMA=0;

	irThreshold=5;			// TODO set via service
	sensorBlocked=false;
	gripperClosed=true;

	rate = 0.1;
	cmdTimer = nh.createTimer(ros::Duration(rate), &IrGripperController::timerCB, this);

	controllerEnabled=false;
}


IrGripperController::~IrGripperController()
{
}

bool IrGripperController::toggle_srv_CB(cortex_msgs::SetBool::Request &req, cortex_msgs::SetBool::Response &res)	// TODO change message type
{
	if(!req.variable)
	{
		controllerEnabled = false;
		ROS_INFO("DISABLED IR gripper controller!");
	}
	else
	{
		controllerEnabled = true;
		ROS_INFO("ENABLED IR gripper controller!");
	}
	return true;
}

void IrGripperController::dataCB(const apc_vacuum::IRSensor::ConstPtr& msg)
{
	// Check size
	if(msg->data.size() != 16)
	{
		ROS_ERROR("IrGripperController::dataCB - received data has wrong size");
		return;
	}

	// Average value
	ir_avg = 0;
	for(int i=0;i<16;i++)
	{
		ir_avg +=msg->data[i];
	}
	ir_avg = ir_avg/16;

	// Compute SMA
	ir_values_indx = (ir_values_indx+1)%window_size;
	SMA = SMA + ir_avg/window_size - ir_values[ir_values_indx]/window_size;
	ir_values[ir_values_indx] = ir_avg;

	// Publish for debugging purposes
	std_msgs::Float64 tmp;
	tmp.data=SMA;
	statusPub1.publish(tmp);
	tmp.data=ir_avg;
	statusPub2.publish(tmp);
}

void IrGripperController::jointStateCB(const sensor_msgs::JointState::Ptr& msg)
{
	int i=0;
	for(std::vector<std::string>::const_iterator it = msg->name.begin(); it != msg->name.end(); ++it)
	{
		if((*it) == "gripper_finger_joint_l")		// gripper_finger_joint_r will have the same value
		{
			if( msg->position[i] < 0.001)
				gripperClosed = true;
			else
				gripperClosed = false;
			return;
		}
		i++;
	}
}

void IrGripperController::timerCB(const ros::TimerEvent&)
{
	if(!controllerEnabled)
		return;

	double diff = ir_avg - SMA;

	if( diff > irThreshold)
	{
		sensorBlocked = true;
	}

	if(sensorBlocked && diff < irThreshold)
	{
		sensorBlocked = false;

		if(gripperClosed)
		{
			std::cout << "Opening Gripper" << std::endl;
			gripper_command.positions[0].value=0.011;
			gripper_command.positions[1].value=0.011;
			gripperClosed=false;
		}
		else
		{
			std::cout << "Closing Gripper" << std::endl;
			gripper_command.positions[0].value=0.0;
			gripper_command.positions[1].value=0.0;
			gripperClosed=true;
		}
		gripperPub.publish(gripper_command);
	}
}


int main(int argc, char** argv)
{
	ros::init(argc,argv,"ir_gripper_controller");
	ros::NodeHandle nh;

	IrGripperController ctrl;

	ros::spin();

	return 0;
}
