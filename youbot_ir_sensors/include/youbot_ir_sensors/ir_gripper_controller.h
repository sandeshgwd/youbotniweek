/*
 * ir_gripper_controller.h
 *
 *  Created on: Jul 30, 2015
 *      Author: Sven Cremer
 */

#ifndef YOUBOT_IR_SENSORS_IR_GRIPPER_CONTROLLER_H_
#define YOUBOT_IR_SENSORS_IR_GRIPPER_CONTROLLER_H_

#include <ros/ros.h>

#include <brics_actuator/JointPositions.h>
#include <boost/units/systems/si.hpp>
#include <boost/units/io.hpp>

#include <std_srvs/Empty.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/JointState.h>
#include <apc_vacuum/IRSensor.h>
#include <cortex_msgs/SetBool.h>

class IrGripperController
{
	private:

		ros::NodeHandle nh;

		ros::Timer cmdTimer;
		ros::Subscriber dataSub;
		ros::Subscriber jointSub;
		ros::Publisher gripperPub;
		ros::Publisher statusPub1;
		ros::Publisher statusPub2;
		ros::ServiceServer toggle_srv;

		void jointStateCB(const sensor_msgs::JointState::Ptr& msg);
		bool toggle_srv_CB(cortex_msgs::SetBool::Request &req, cortex_msgs::SetBool::Response &res);
		void dataCB(const apc_vacuum::IRSensor::ConstPtr& msg);
		void timerCB(const ros::TimerEvent&);

		bool controllerEnabled;
		double rate;
		brics_actuator::JointPositions gripper_command;

//		double ir_sensor1[4][4];			// Received IR data
//		double ir_sensor2[4][4];
//		double ir_sensor3[4][4];

		double ir_avg;						// Average sensor value
		double SMA;							// Simple moving average
		std::vector<double> ir_values;
		int ir_values_indx;
		int window_size;

		double irThreshold;
		bool sensorBlocked;
		bool gripperClosed;

	public:

		IrGripperController();
		~IrGripperController();

};



#endif /* YOUBOT_IR_SENSORS_IR_GRIPPER_CONTROLLER_H_ */
