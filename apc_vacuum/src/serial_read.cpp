#include "serial/serial.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <string.h>
#include <sstream> 
#include <iostream>
#include <cstdio>
#include "apc_vacuum/ValveCtrl.h"
#include <sensor_msgs/JointState.h>
#include <apc_vacuum/forceSensor.h>
#include <apc_vacuum/IRSensor.h>
// Testing for sensor module
#include <geometry_msgs/Quaternion.h>

// OS Specific sleep
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

const char seps[]   = " ,\t\n";

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::stringstream;

 // port, baudrate, timeout in milliseconds
 serial::Serial my_serial("/dev/ttyUSB0", 9600, serial::Timeout::simpleTimeout(2000));

ros::Subscriber jointStatus_sub_sandy;

std::string send_command;

void jointTransmit(const sensor_msgs::JointState::ConstPtr& msg)
{
	if(msg->name[0] == "arm_joint_1")
	{
		std::ostringstream str1,str2;
		std::string temp_str;
		str1 << msg->position[0];
		send_command = str1.str();
		for(int i = 1; i<7 ; i++)
		{
			str2.str("");
			str2.clear();
			str2 << msg->position[i];
			temp_str = str2.str();
			send_command = send_command + "," + temp_str;
		}
	}
}


// Wait for miliseconds
void my_sleep(unsigned long milliseconds) 
{
#ifdef _WIN32
      Sleep(milliseconds);
#else
      usleep(milliseconds*1000);
#endif
}

int run(int argc, char **argv)
{
	apc_vacuum::forceSensor force;
	geometry_msgs::Quaternion pressurePoints;
	force.data.resize(4);
	apc_vacuum::IRSensor IR;
	IR.data.resize(IR.row*IR.col);

  string pressure_data;		// Contain the pressure data from arduino
  std_msgs::String sensor;
  ros::init(argc, argv, "SerialCom");
  ros::NodeHandle n;
  
  jointStatus_sub_sandy =  n.subscribe<sensor_msgs::JointState>("joint_states", 1, jointTransmit);
  
  ros::Publisher serial_pub = n.advertise<std_msgs::String>("serial", 1);
  ros::Publisher force_pub  = n.advertise<apc_vacuum::forceSensor>("force_sensor_data",1);
	//single sensor testing rqt_plot
	ros::Publisher pressure_pub  = n.advertise<geometry_msgs::Quaternion>("pressure_data",1);
  ros::Publisher IR_pub     = n.advertise<apc_vacuum::IRSensor>("IR_sensor_data",1);
  ros::Rate loop_rate(120);  // Loop will run at 10hz
  
  //ros::ServiceServer service = n.advertiseService("valve_control", valve_ctrl_cmd);




  cout << "Is the serial port open?";
  if(my_serial.isOpen())
    cout << " Yes." << endl;
  else
    cout << " No." << endl;

  my_serial.readline(pressure_data);

	send_command = "0,0,0,0,0,0,0";

  while (ros::ok())
  {
	  my_serial.write(send_command);
//	  std::cout << send_command;
//	  std::cout << "Beginning Loop" << std::endl;
	  size_t bytes_read = my_serial.readline(pressure_data);

 //   std::cout << "Bytes read: " << bytes_read << std::endl;
 //   std::cout << "" << pressure_data << std::endl;
    
    //sensor.data = pressure_data;
	if(pressure_data.length() == 114)
	{
    char* token = strtok(const_cast<char*>(pressure_data.c_str()),seps);


		for(int pressure_index = 0; pressure_index < force.num ; pressure_index++)
    {
		force.data[pressure_index] = (double)atof(token);
		token=strtok(NULL,seps);
    }
		for (int ir_index = 0; ir_index < IR.row * IR.col ; ir_index++)
		{
				IR.data[ir_index]=atoi(token);
				token=strtok(NULL,seps);
		}
	}
	// Single sensor testing rqt_plot
	pressurePoints.x = force.data[0];
	pressurePoints.y = force.data[1];
	pressurePoints.z = force.data[2];
	pressurePoints.w = force.data[3];

		pressure_pub.publish(pressurePoints);
		force_pub.publish(force);
		IR_pub.publish(IR);
		//serial_pub.publish(sensor);
		
		pressure_data.clear();
    ros::spinOnce();
		loop_rate.sleep();
    
  }
  cout << "\nClosing Serial Port.\n";
  my_serial.close();


  return 0;
}

int main(int argc, char **argv) 
{
  do
  {
	try
	{
		run(argc, argv);
	}
	catch (exception &e)
	{
		cerr <<"Unhandled Exception: " << e.what() << endl;
		my_serial.close();
	}
	my_sleep(500);
	my_serial.open();


  }while(1);
	
	return 0;
}

